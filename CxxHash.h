/*
 * CxxHash - C++ Wrapper for xxHash (Extremely Fast Hash algorithm)
 * Header File
 * Copyright © 2023-2023 Pedro Miguel Carvalho <PedroMC@pmc.com.pt>
 *
 * BSD 2-Clause License (https://www.opensource.org/licenses/bsd-license.php)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the
 *      distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You can contact the author at: mailto:PedroMC@pmc.com.pt?subject=CxxHash
 */

#pragma once
#ifndef CXXHASH_H
#define CXXHASH_H

#include <algorithm>
#include <array>
#include <cstddef>
#include <new>
#include <span>
#include <stdexcept>
#include <xxhash.h>

#include "Hexadecimal.h"

namespace CxxHash {

template< typename DATA_T >
concept Spanable_t = requires(const DATA_T& t) {
    {t.data()} -> std::convertible_to<const void*>;
    {t.size()} -> std::convertible_to<std::size_t>;
    typename DATA_T::value_type;
};

template< Spanable_t DATA_T >
[[nodiscard]] inline std::span<const char> to_span(const DATA_T& data) {
    return std::span{static_cast<const char*>(static_cast<const void*>(data.data())), data.size() * sizeof(typename DATA_T::value_type)};
}

template< typename HASH_T
        , typename SEED_T
        , typename CANONICAL_T
        , typename STATE_T
        , STATE_T*      (*CREATE_STATE_FUNC        )()
        , XXH_errorcode (*FREE_STATE_FUNC          )(STATE_T*)
        , void          (*COPY_STATE_FUNC          )(STATE_T*, const STATE_T*)
        , XXH_errorcode (*RESET_FUNC               )(STATE_T*, SEED_T)
        , XXH_errorcode (*UPDATE_FUNC              )(STATE_T*, const void*, size_t)
        , HASH_T        (*DIGEST_FUNC              )(const STATE_T*)
        , void          (*CANONICAL_FROM_VALUE_FUNC)(CANONICAL_T*, HASH_T)
        , HASH_T        (*HASH_FROM_CANONICAL_FUNC )(const CANONICAL_T*)
        >
class CxxHash {
public:

    using Hash_t      = HASH_T;
    using Seed_t      = SEED_T;
    using Canonical_t = CANONICAL_T;
    using State_t     = STATE_T;

    class Hash {
    public:
        static constexpr size_t size = sizeof(Canonical_t::digest);
        using Array_t = std::array<unsigned char, size>;

        constexpr Hash() noexcept = default;
        constexpr Hash(const Hash&) noexcept = default;
        constexpr Hash(Hash&&) noexcept = default;
        constexpr explicit Hash(const Hash_t hash) noexcept : _hash{hash} {}
        constexpr explicit Hash(const Array_t& array) noexcept : Hash{_to_hash(array)} {}
        constexpr explicit Hash(const Hexadecimal& hexadecimal) : Hash{_to_hash(hexadecimal)} {}

        constexpr ~Hash() noexcept = default;

        constexpr Hash& operator =(const Hash&) noexcept = default;
        constexpr Hash& operator =(Hash&&) noexcept = default;

        [[nodiscard]] constexpr bool operator==(const Hash& hash) const noexcept = default;
        [[nodiscard]] constexpr auto operator<=>(const Hash& hash) const noexcept = default;

        [[nodiscard]] constexpr Hash_t get_value() const noexcept {
            return _hash;
        }

        [[nodiscard]] constexpr Canonical_t get_canonical() const noexcept {
            Canonical_t canonical;
            CANONICAL_FROM_VALUE_FUNC(&canonical, _hash);
            return canonical;
        }

        [[nodiscard]] constexpr Array_t get_array() const noexcept {
            return std::to_array(get_canonical().digest);
        }

        [[nodiscard]] Hexadecimal get_hexadecimal() const noexcept {
            return Hexadecimal{get_canonical().digest};
        }

    private:

        [[nodiscard]] static constexpr Hash_t _to_hash(const Array_t& array) noexcept {
            Canonical_t canonical;
            std::copy(array.cbegin(), array.cend(), canonical.digest);
            return HASH_FROM_CANONICAL_FUNC(& canonical);
        }

        [[nodiscard]] static constexpr Hash_t _to_hash(const Hexadecimal& hexadecimal) {
            Canonical_t canonical;
            hexadecimal.to(canonical.digest);
            return HASH_FROM_CANONICAL_FUNC(& canonical);
        }

        Hash_t _hash;

    };

    explicit CxxHash(const Hash seed = {})
        : CxxHash{nullptr} {
        reset(seed);
    }

    CxxHash(const CxxHash& xxh)
        : CxxHash{nullptr} {
        COPY_STATE_FUNC(_state, xxh._state);
    }

    CxxHash(CxxHash&& xxh) noexcept
        : _state{xxh._state} {
        xxh._state = nullptr;
    }

    ~CxxHash() {
        FREE_STATE_FUNC(_state);
    }

    CxxHash& operator=(const CxxHash& xxh) noexcept {
        if(this != &xxh) {
            COPY_STATE_FUNC(_state, xxh._state);
        }
        return *this;
    }

    CxxHash& operator=(CxxHash&& xxh) noexcept {
        std::swap(_state, xxh._state);
        return *this;
    }

    CxxHash& reset(const Hash seedHash) {
        const Seed_t seed = _hash_to_seed(seedHash.get_value());
        if(RESET_FUNC(_state, seed) == XXH_ERROR) {
            throw std::runtime_error{"CxxHash: Failed to reset state."};
        }
        return *this;
    }

    CxxHash& update(const std::span<const char> data) {
        if(UPDATE_FUNC(_state, data.data(), data.size_bytes()) == XXH_ERROR) {
            throw std::runtime_error{"CxxHash: Failed to update state."};
        }
        return *this;
    }

    template< typename DATA_T >
    [[nodiscard]] CxxHash& update(const DATA_T& data) {
        return update(to_span(data));
    }

    [[nodiscard]] Hash get_hash() const noexcept {
        return Hash{DIGEST_FUNC(_state)};
    }

    [[nodiscard]] static inline const char* name() noexcept {
        return _name;
    }

    [[nodiscard]] static Hash calculate(const std::span<const char> data, const Hash seed = {}) {
        return CxxHash{seed}.update(data).get_hash();
    }

    template< typename DATA_T >
    [[nodiscard]] static Hash calculate(const DATA_T& data, const Hash seed = {}) {
        return calculate(to_span(data), seed);
    }

private:

    explicit CxxHash(std::nullptr_t)
        : _state{CREATE_STATE_FUNC()} {
        if(_state == nullptr) {
            throw std::bad_alloc{};
        }
    }

    [[nodiscard]] static inline Seed_t _hash_to_seed(const Hash_t hash) noexcept {
        static_assert(std::is_same_v<Hash_t, Seed_t>, "Specialization required if HASH_T is different from SEED_T and not explicitly convertible.");
        return Seed_t{hash};
    }

    State_t* _state;

    // Must specialize _name for each specific CxxHash.
    static const char* const _name;

};

using CxxHash_32 = CxxHash< XXH32_hash_t
                          , XXH32_hash_t
                          , XXH32_canonical_t
                          , XXH32_state_t
                          , XXH32_createState
                          , XXH32_freeState
                          , XXH32_copyState
                          , XXH32_reset
                          , XXH32_update
                          , XXH32_digest
                          , XXH32_canonicalFromHash
                          , XXH32_hashFromCanonical
                          >;

using CxxHash_64 = CxxHash< XXH64_hash_t
                          , XXH64_hash_t
                          , XXH64_canonical_t
                          , XXH64_state_t
                          , XXH64_createState
                          , XXH64_freeState
                          , XXH64_copyState
                          , XXH64_reset
                          , XXH64_update
                          , XXH64_digest
                          , XXH64_canonicalFromHash
                          , XXH64_hashFromCanonical
                          >;

using CxxHash3_64 = CxxHash< XXH64_hash_t
                           , XXH64_hash_t
                           , XXH64_canonical_t
                           , XXH3_state_t
                           , XXH3_createState
                           , XXH3_freeState
                           , XXH3_copyState
                           , XXH3_64bits_reset_withSeed
                           , XXH3_64bits_update
                           , XXH3_64bits_digest
                           , XXH64_canonicalFromHash
                           , XXH64_hashFromCanonical
                           >;

using CxxHash3_128 = CxxHash< XXH128_hash_t
                            , XXH64_hash_t
                            , XXH128_canonical_t
                            , XXH3_state_t
                            , XXH3_createState
                            , XXH3_freeState
                            , XXH3_copyState
                            , XXH3_128bits_reset_withSeed
                            , XXH3_128bits_update
                            , XXH3_128bits_digest
                            , XXH128_canonicalFromHash
                            , XXH128_hashFromCanonical
                            >;

template<> constexpr const char* const CxxHash_32  ::_name = "XXH_32";
template<> constexpr const char* const CxxHash_64  ::_name = "XXH_64";
template<> constexpr const char* const CxxHash3_64 ::_name = "XXH3_64";
template<> constexpr const char* const CxxHash3_128::_name = "XXH3_128";

template<>
[[nodiscard]] inline CxxHash3_128::Seed_t CxxHash3_128::_hash_to_seed(const CxxHash3_128::Hash_t hash) noexcept {
    return hash.high64;
}

} // namespace CxxHash

[[nodiscard]] inline constexpr bool operator==(const CxxHash::CxxHash3_128::Hash_t hash_a, const CxxHash::CxxHash3_128::Hash_t hash_b) noexcept {
    return hash_a.high64 == hash_b.high64 && hash_a.low64 == hash_b.low64;
}

[[nodiscard]] inline constexpr auto operator<=>(const CxxHash::CxxHash3_128::Hash_t hash_a, const CxxHash::CxxHash3_128::Hash_t hash_b) noexcept {
    const bool hab = hash_a.high64 > hash_b.high64;
    const bool hba = hash_b.high64 > hash_a.high64;
    const bool lab = hash_a.low64 > hash_b.low64;
    const bool lba = hash_b.low64 > hash_a.low64;
    return (static_cast<int>(hab) - static_cast<int>(hba)) * 2 + (static_cast<int>(lab) - static_cast<int>(lba));
}

#endif // CXXHASH_H
