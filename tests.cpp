/*
 * CxxHash - C++ Wrapper for xxHash (Extremely Fast Hash algorithm)
 * Header File
 * Copyright © 2023-2023 Pedro Miguel Carvalho <PedroMC@pmc.com.pt>
 *
 * BSD 2-Clause License (https://www.opensource.org/licenses/bsd-license.php)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the
 *      distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You can contact the author at: mailto:PedroMC@pmc.com.pt?subject=CxxHash
 */

#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_RUNNER
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch.hpp>

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <limits>
#include <random>
#include <span>
#include <string>
#include <string_view>
#include <stdexcept>
#include <vector>

#include "Hexadecimal.h"
#include "CxxHash.h"

using namespace CxxHash;

TEST_CASE( "Hexadecimal ctor", "[Hexadecimal]" ) {
    SECTION("default ctor") {
        CHECK( Hexadecimal{} == Hexadecimal{} );
        CHECK( Hexadecimal{}.get_string().empty() );
    }

    SECTION("copy ctor") {
        const auto string = GENERATE(
            std::string{""},
            std::string{"ad"},
            std::string{"4af4"},
            std::string{"2a34ce"},
            std::string{"2a30602c"}
            );

        const Hexadecimal hexadecimal{string};

        CHECK( Hexadecimal{string} == Hexadecimal{hexadecimal} );
        CHECK( Hexadecimal{string} == Hexadecimal{Hexadecimal{string}} );
    }

    SECTION("hexa string ctor") {
        const auto string = GENERATE(
            std::string{""},
            std::string{"ad"},
            std::string{"4af4"},
            std::string{"2a34ce"},
            std::string{"2a30602c"}
            );

        CHECK( Hexadecimal::is_valid_hexadecimal(string) );
        CHECK( string == Hexadecimal{string}.get_string() );
        CHECK( Hexadecimal{string} == Hexadecimal{string} );
    }

    SECTION("vector -> span ctor -> vector") {
        const auto array = GENERATE(
            std::vector<unsigned char>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            std::vector<unsigned char>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
            std::vector<unsigned char>{15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
            std::vector<unsigned char>{29, 145, 7, 246, 0, 201, 32, 93, 140, 21, 43, 124, 252, 9, 117, 163}
            );

        std::vector<unsigned char> out_array(array.size());
        Hexadecimal{array}.to(out_array);

        CHECK( array ==  out_array);
        CHECK( Hexadecimal{array}.data_size() == array.size() );
        CHECK( Hexadecimal{array}.size() == array.size() * 2 );
        CHECK( Hexadecimal{array} == Hexadecimal{array} );
        CHECK( Hexadecimal::is_valid_hexadecimal(Hexadecimal{array}.get_string()) );
    }

    SECTION("vector -> from -> vector") {
        const auto array = GENERATE(
            std::vector<unsigned char>{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            std::vector<unsigned char>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
            std::vector<unsigned char>{15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
            std::vector<unsigned char>{29, 145, 7, 246, 0, 201, 32, 93, 140, 21, 43, 124, 252, 9, 117, 163}
            );

        Hexadecimal hexadecimal;
        hexadecimal.from(array);
        std::vector<unsigned char> out_array(array.size());
        hexadecimal.to(out_array);

        CHECK( array ==  out_array);
        CHECK( hexadecimal.data_size() == array.size() );
        CHECK( hexadecimal.size() == array.size() * 2 );
        CHECK( hexadecimal == hexadecimal );
        CHECK( Hexadecimal::is_valid_hexadecimal(hexadecimal.get_string()) );
    }

    SECTION("invalid hexa string ctor") {
        const auto string = GENERATE(
            std::string{"a"},
            std::string{"ada"},
            std::string{"4af4a"},
            std::string{"2a34cea"},
            std::string{"2a30602ca"},
            std::string{"ZZ"},
            std::string{"adaZZ"},
            std::string{"4af4aZZ"},
            std::string{"2a34ceaZZ"},
            std::string{"2a30602caZZ"}
            );

        CHECK_FALSE( Hexadecimal::is_valid_hexadecimal(string) );
        CHECK_THROWS_MATCHES( Hexadecimal{string}, std::invalid_argument, Catch::Matchers::Message("Invalid hexadecimal string.") );
    }
}

TEST_CASE( "CxxHash*::name()", "[CxxHash]" ) {
    CHECK( std::string{CxxHash_32  ::name()} == std::string{"XXH_32"  } );
    CHECK( std::string{CxxHash_64  ::name()} == std::string{"XXH_64"  } );
    CHECK( std::string{CxxHash3_64 ::name()} == std::string{"XXH3_64" } );
    CHECK( std::string{CxxHash3_128::name()} == std::string{"XXH3_128"} );
}

TEST_CASE( "CxxHash_32::Hash", "[CxxHash]" ) {
    SECTION("default ctor") {
        CHECK( CxxHash_32::Hash{} == CxxHash_32::Hash{} );
        CHECK( 0 == CxxHash_32::Hash{}.get_value());
    }

    SECTION("value ctor") {
        const auto value = CxxHash_32::Hash_t{GENERATE(0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 63276387U, 2482649U, 1074561U, 817637832U)};
        CHECK( CxxHash_32::Hash{value} == CxxHash_32::Hash{value} );
        CHECK( value == CxxHash_32::Hash{value}.get_value());
    }

    SECTION("array ctor") {
        const auto array = GENERATE(
            CxxHash_32::Hash::Array_t{0, 0, 0, 0},
            CxxHash_32::Hash::Array_t{1, 2, 3, 4},
            CxxHash_32::Hash::Array_t{9, 8, 7, 6},
            CxxHash_32::Hash::Array_t{29, 145, 7, 246}
            );

        CHECK( array == CxxHash_32::Hash{array}.get_array() );
        CHECK( CxxHash_32::Hash{array} == CxxHash_32::Hash{array} );
    }

    SECTION("hexa string ctor") {
        const auto hexadecimal = Hexadecimal{GENERATE(
            std::string{"c8931068"},
            std::string{"adf8e9e9"},
            std::string{"4af4265a"},
            std::string{"2a34ce5f"},
            std::string{"2a30602c"}
            )};

        CHECK( hexadecimal == CxxHash_32::Hash{hexadecimal}.get_hexadecimal() );
        CHECK( CxxHash_32::Hash{hexadecimal} == CxxHash_32::Hash{hexadecimal} );
    }

    SECTION("invalid hexa string ctor") {
        struct Test_Data {
            std::string string;
            std::string message;
        };

        const auto test_data = GENERATE(
            Test_Data {
                "c89310",
                "Invalid data size."
            },
            Test_Data {
                "c8931068aa",
                "Invalid data size."
            },
            Test_Data {
                "",
                "Invalid data size."
            },
            Test_Data {
                "c_931068",
                "Invalid hexadecimal string."
            },
            Test_Data {
                "XYZ31068",
                "Invalid hexadecimal string."
            }
            );

        CHECK_THROWS_MATCHES( CxxHash_32::Hash{Hexadecimal{test_data.string}}, std::invalid_argument, Catch::Matchers::Message(test_data.message) );
    }
}

TEST_CASE( "CxxHash_64::Hash", "[CxxHash]" ) {
    SECTION("default ctor") {
        CHECK( CxxHash_64::Hash{} == CxxHash_64::Hash{} );
        CHECK( 0 == CxxHash_64::Hash{}.get_value());
    }

    SECTION("value ctor") {
        const auto value = CxxHash_64::Hash_t{GENERATE(0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 63276387U, 2482649U, 1074561U, 817637832U, 3443653563456345U, 7325698347637264U, 9867252387467233U)};
        CHECK( CxxHash_64::Hash{value} == CxxHash_64::Hash{value} );
        CHECK( value == CxxHash_64::Hash{value}.get_value());
    }

    SECTION("array ctor") {
        const auto array = GENERATE(
            CxxHash_64::Hash::Array_t{0, 0, 0, 0, 0, 0, 0, 0},
            CxxHash_64::Hash::Array_t{1, 2, 3, 4, 5, 6, 7, 8},
            CxxHash_64::Hash::Array_t{9, 8, 7, 6, 5, 4, 3, 2},
            CxxHash_64::Hash::Array_t{29, 145, 7, 246, 0, 201, 32, 93}
            );

        CHECK( array == CxxHash_64::Hash{array}.get_array() );
        CHECK( CxxHash_64::Hash{array} == CxxHash_64::Hash{array} );
    }

    SECTION("hexa string ctor") {
        const auto hexadecimal = Hexadecimal{GENERATE(
            std::string{"c8931068f85b25df"},
            std::string{"adf8e9e9d2058da0"},
            std::string{"4af4265aee35f57f"},
            std::string{"2a34ce5f2f590a81"},
            std::string{"2a30602cf5469859"}
            )};

        CHECK( hexadecimal == CxxHash_64::Hash{hexadecimal}.get_hexadecimal() );
        CHECK( CxxHash_64::Hash{hexadecimal} == CxxHash_64::Hash{hexadecimal} );
    }

    SECTION("invalid hexa string ctor") {
        struct Test_Data {
            std::string string;
            std::string message;
        };

        const auto test_data = GENERATE(
            Test_Data {
                "c8931068f85b25",
                "Invalid data size."
            },
            Test_Data {
                "c8931068f85b25dfaa",
                "Invalid data size."
            },
            Test_Data {
                "",
                "Invalid data size."
            },
            Test_Data {
                "c_931068f85b25df",
                "Invalid hexadecimal string."
            },
            Test_Data {
                "XYZ31068f85b25df",
                "Invalid hexadecimal string."
            }
            );

        CHECK_THROWS_MATCHES( CxxHash_64::Hash{Hexadecimal{test_data.string}}, std::invalid_argument, Catch::Matchers::Message(test_data.message) );
    }
}

TEST_CASE( "CxxHash3_64::Hash", "[CxxHash]" ) {
    SECTION("default ctor") {
        CHECK( CxxHash3_64::Hash{} == CxxHash3_64::Hash{} );
        CHECK( 0 == CxxHash3_64::Hash{}.get_value());
    }

    SECTION("value ctor") {
        const auto value = CxxHash3_64::Hash_t{GENERATE(0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 63276387U, 2482649U, 1074561U, 817637832U, 3443653563456345U, 7325698347637264U, 9867252387467233U)};
        CHECK( CxxHash3_64::Hash{value} == CxxHash3_64::Hash{value} );
        CHECK( value == CxxHash3_64::Hash{value}.get_value());
    }

    SECTION("array ctor") {
        const auto array = GENERATE(
            CxxHash3_64::Hash::Array_t{0, 0, 0, 0, 0, 0, 0, 0},
            CxxHash3_64::Hash::Array_t{1, 2, 3, 4, 5, 6, 7, 8},
            CxxHash3_64::Hash::Array_t{9, 8, 7, 6, 5, 4, 3, 2},
            CxxHash3_64::Hash::Array_t{29, 145, 7, 246, 0, 201, 32, 93}
        );

        CHECK( array == CxxHash3_64::Hash{array}.get_array() );
        CHECK( CxxHash3_64::Hash{array} == CxxHash3_64::Hash{array} );
    }

    SECTION("hexa string ctor") {
        const auto hexadecimal = Hexadecimal{GENERATE(
            std::string{"c8931068f85b25df"},
            std::string{"adf8e9e9d2058da0"},
            std::string{"4af4265aee35f57f"},
            std::string{"2a34ce5f2f590a81"},
            std::string{"2a30602cf5469859"}
        )};

        CHECK( hexadecimal == CxxHash3_64::Hash{hexadecimal}.get_hexadecimal() );
        CHECK( CxxHash3_64::Hash{hexadecimal} == CxxHash3_64::Hash{hexadecimal} );
    }

    SECTION("invalid hexa string ctor") {
        struct Test_Data {
            std::string string;
            std::string message;
        };

        const auto test_data = GENERATE(
            Test_Data {
                "c8931068f85b25",
                "Invalid data size."
            },
            Test_Data {
                "c8931068f85b25dfaa",
                "Invalid data size."
            },
            Test_Data {
                "",
                "Invalid data size."
            },
            Test_Data {
                "c_931068f85b25df",
                "Invalid hexadecimal string."
            },
            Test_Data {
                "XYZ31068f85b25df",
                "Invalid hexadecimal string."
            }
            );

        CHECK_THROWS_MATCHES( CxxHash3_64::Hash{Hexadecimal{test_data.string}}, std::invalid_argument, Catch::Matchers::Message(test_data.message) );
    }
}

TEST_CASE( "CxxHash3_128::Hash", "[CxxHash]" ) {
    SECTION("default ctor") {
        CHECK( CxxHash3_128::Hash{} == CxxHash3_128::Hash{} );
        CHECK( CxxHash3_128::Hash_t{0, 0} == CxxHash3_128::Hash{}.get_value() );
    }

    SECTION("value ctor") {
        const auto value_l = CxxHash_64::Hash_t{GENERATE(0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 63276387U, 2482649U, 1074561U, 817637832U, 3443653563456345U, 7325698347637264U, 9867252387467233U)};
        const auto value_h = CxxHash_64::Hash_t{GENERATE(0U, 1U, 2U, 3U, 4U, 5U, 6U, 7U, 8U, 9U, 63276387U, 2482649U, 1074561U, 817637832U, 3443653563456345U, 7325698347637264U, 9867252387467233U)};
        const auto value = CxxHash3_128::Hash_t{value_h, value_l};
        CHECK( CxxHash3_128::Hash{value} == CxxHash3_128::Hash{value} );
        CHECK( value == CxxHash3_128::Hash{value}.get_value() );
    }

    SECTION("array ctor") {
        const auto array = GENERATE(
            CxxHash3_128::Hash::Array_t{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            CxxHash3_128::Hash::Array_t{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
            CxxHash3_128::Hash::Array_t{15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
            CxxHash3_128::Hash::Array_t{29, 145, 7, 246, 0, 201, 32, 93, 140, 21, 43, 124, 252, 9, 117, 163}
            );

        CHECK( array == CxxHash3_128::Hash{array}.get_array() );
        CHECK( CxxHash3_128::Hash{array} == CxxHash3_128::Hash{array} );
    }

    SECTION("hexa string ctor") {
        const auto hexadecimal = Hexadecimal{GENERATE(
            std::string{"c8931068f85b25df35a5af408892585c"},
            std::string{"adf8e9e9d2058da0b99770686786cc36"},
            std::string{"4af4265aee35f57fe8a72f41fead8545"},
            std::string{"2a34ce5f2f590a813792a6a1d3eb50e7"},
            std::string{"2a30602cf54698598aaada74992a1de9"}
            )};

        CHECK( hexadecimal == CxxHash3_128::Hash{hexadecimal}.get_hexadecimal() );
        CHECK( CxxHash3_128::Hash{hexadecimal} == CxxHash3_128::Hash{hexadecimal} );
    }

    SECTION("invalid hexa string ctor") {
        struct Test_Data {
            std::string string;
            std::string message;
        };

        const auto test_data = GENERATE(
            Test_Data {
                "2a30602cf54698598aaada74992a1d",
                "Invalid data size."
            },
            Test_Data {
                "2a30602cf54698598aaada74992a1de9aa",
                "Invalid data size."
            },
            Test_Data {
                "",
                "Invalid data size."
            },
            Test_Data {
                "2_30602cf54698598aaada74992a1de9",
                "Invalid hexadecimal string."
            },
            Test_Data {
                "XYZ0602cf54698598aaada74992a1de9",
                "Invalid hexadecimal string."
            }
        );

        CHECK_THROWS_MATCHES( CxxHash3_128::Hash{Hexadecimal{test_data.string}}, std::invalid_argument, Catch::Matchers::Message(test_data.message) );
    }
}

TEST_CASE( "CxxHash_*::calculate()", "[CxxHash]" ) {
    struct Test_Data {
        std::string raw_data;
        Hexadecimal xxh_32;
        Hexadecimal xxh_64;
        Hexadecimal xxh3_64;
        Hexadecimal xxh3_128;
    };

    const auto test_data = GENERATE(
        Test_Data{
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef"
            "0123456789abcdef",
            Hexadecimal{"2a30602c"},
            Hexadecimal{"3792a6a1d3eb50e7"},
            Hexadecimal{"2a34ce5f2f590a81"},
            Hexadecimal{"4af4265aee35f57fe8a72f41fead8545"}
        },
        Test_Data{
            "0000000 5f99 dba3 9151 d826 0657 1a11 65a1 5d7a\n"
            "0000010 1c0c f705 1e44 23f4 de6c 63bf 1a3f a5ce\n"
            "0000020 65aa 7cad 2822 5fb5 9446 ca8c 1249 b421\n"
            "0000030 e40b d1e1 18e8 fc64 5683 cd3a 1048 2f47\n"
            "0000040 16f3 f796 e163 73ba 2544 c725 06a6 92cc\n"
            "0000050 c37d e0b8 9f89 08a0 cbf5 a2d4 ecb7 3eba\n"
            "0000060 89b4 be41 5060 4e3d 9a32 2357 d470 3284\n"
            "0000070 e5da a93e a11f 356a aae7 dd15 041d 7534\n",
            Hexadecimal{"f5469859"},
            Hexadecimal{"a646d51ab4e64c0a"},
            Hexadecimal{"b99770686786cc36"},
            Hexadecimal{"adf8e9e9d2058da0b99770686786cc36"}
        },
        Test_Data{
            "AAQTdstY4FRyQQcLeA6YUS5FEev7uPHe8YXTzfCRDcDRWQJDbFGSmb7gya8tULG7VMja/bftDXeV\n"
            "u/G8QQ4GYvXcqTuQZLfEE3OWKvMi7hybUj7SgCUyiVJMGqirxC9rBD3VBNO6WPxdkPLErke4F3vP\n"
            "/E36yNdTFal7tDRFmDxszWQx/spUeuSSbaMHOqHjDdM0cuop2w2qsxvQ5aCIz3Qm1DWZZl76nluP\n"
            "os+9ZczzMyfZPWcAjeQeQw2mWMKm3h/5AbzOsgdlGEYo1Gby1RJPQ8Oix2bZ77KkkG6yz4TLBTFK\n"
            "kMMULeI5jOcbVLx7k9HOHBuawXCXqKlM8KDqXg==\n",
            Hexadecimal{"8072d552"},
            Hexadecimal{"8aaada74992a1de9"},
            Hexadecimal{"35a5af408892585c"},
            Hexadecimal{"c8931068f85b25df35a5af408892585c"}
        },
        Test_Data{
            "6yGcTDJPiQIOvaPiIgroNqFeB7ftKS687QgyeV/J4ws+WFZWMS9qlO3EKrjkmp/KQkvrOGK5QQCx\n"
            "ONCi0adhsvT93hzysnm0yPVSGyN4WIfkZfX3C+NltwpiUu+jvt9Jmn78JPDthOPPL2L8Moyg6cDe\n"
            "QAJp6KeetSRGG3doGlmmagjmyN+Keh0IicQazctoBsZqzAePEPapKmTg3STYehFr/E6pqXGyc+Gr\n"
            "N3S3Rm4sH4Cfq1AnhKYv3q186gi9W1oiD0gzRpO5iFMT9TgzeU1hrqjDX6cCG91IUU3Exwi5IDPp\n"
            "W22Mg3iYYdh/PjyNOqv2dIoDTVrngeiUXSeZdtpunbVk+JpqNDPcx6XHIsqEAhw1ju2+ofLEqWy8\n"
            "vCWgwMGG2qzI7qtXik7kIgoIpdrfM4IUPNHdg+bgUnMi1XDJeqjBfN7d+Eybs+sZDEkPuO8cRbZE\n"
            "YWFZH8EQF2QtDyreDRHW2zQ8uXtPLsBY21HxJnLeYp40mAYkYKi48Jd1E3N+lm8oEquwGbTzl++Z\n"
            "3cOalgge0C11sEdD6b+LEv3MJWKAhrxAcTiZoeAGeT7uxR1+iK6mX0eMxp0k5IrdowD2IGkL0ySr\n"
            "+t5vjwporhvh2tgnnaOW9Wb0UzCoeXIGOnFCMs93SQ96ws0K5In1VPq2mjBWAXiki86d2BZhDwU=\n",
            Hexadecimal{"7589da39"},
            Hexadecimal{"cce9ca203f456d66"},
            Hexadecimal{"367f7a874f13a65a"},
            Hexadecimal{"42b7b321db1d35db367f7a874f13a65a"}
        }
    );

    CHECK( CxxHash_32  ::calculate(test_data.raw_data).get_hexadecimal() == test_data.xxh_32   );
    CHECK( CxxHash_64  ::calculate(test_data.raw_data).get_hexadecimal() == test_data.xxh_64   );
    CHECK( CxxHash3_64 ::calculate(test_data.raw_data).get_hexadecimal() == test_data.xxh3_64  );
    CHECK( CxxHash3_128::calculate(test_data.raw_data).get_hexadecimal() == test_data.xxh3_128 );
}

TEST_CASE("CxxHash3_128::operator<=>", "[CxxHash]") {
    struct Test_Data {
        CxxHash3_128::Hash_t h1;
        CxxHash3_128::Hash_t h2;
        int cmp_sign;
    };

    const auto test_data = GENERATE(
        Test_Data{
            {0, 0},
            {0, 0},
            0
        },
        Test_Data{
            {1, 1},
            {1, 1},
            0
        },
        Test_Data{
            {0, 1},
            {0, 0},
            +1
        },
        Test_Data{
            {0, 1},
            {1, 0},
            +1
        },
        Test_Data{
            {1, 1},
            {0, 0},
            +1
        },
        Test_Data{
            {0, 0},
            {0, 1},
            -1
        },
        Test_Data{
            {1, 0},
            {0, 1},
            -1
        },
        Test_Data{
            {0, 1},
            {1, 1},
            -1
        }
    );

    const int cmp = test_data.h1 <=> test_data.h2;
    const int cmp_r = test_data.h2 <=> test_data.h1;
    CHECK( std::abs(cmp) * test_data.cmp_sign == cmp );
    CHECK( cmp_r == - cmp );
}

struct Test_t {
    std::string str;
};

[[nodiscard]] inline std::span<const char> to_span(const Test_t& data) {
    return to_span(data.str);
}

TEST_CASE( "CxxHash_*::calculate(<multitype>)", "[CxxHash]" ) {

    SECTION("char data") {
        const char*                cstring  = "test 1 2 3";
        const std::string_view     csview     {cstring};
        const char                 carray[10] {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::span            cspan      {carray};
        const std::string          string     {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::vector<char>    vector     {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::array<char, 10> array      {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const Test_t               test      {{'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'}};

        CHECK( CxxHash_32  ::calculate(csview).get_hexadecimal() == Hexadecimal{"e100d608"} );
        CHECK( CxxHash_32  ::calculate(cspan ).get_hexadecimal() == Hexadecimal{"e100d608"} );
        CHECK( CxxHash_32  ::calculate(string).get_hexadecimal() == Hexadecimal{"e100d608"} );
        CHECK( CxxHash_32  ::calculate(vector).get_hexadecimal() == Hexadecimal{"e100d608"} );
        CHECK( CxxHash_32  ::calculate(array ).get_hexadecimal() == Hexadecimal{"e100d608"} );
        CHECK( CxxHash_32  ::calculate(test  ).get_hexadecimal() == Hexadecimal{"e100d608"} );

        CHECK( CxxHash_64  ::calculate(csview).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( CxxHash_64  ::calculate(cspan ).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( CxxHash_64  ::calculate(string).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( CxxHash_64  ::calculate(vector).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( CxxHash_64  ::calculate(array ).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( CxxHash_64  ::calculate(test  ).get_hexadecimal() == Hexadecimal{"4c54f58f73c928c3"} );

        CHECK( CxxHash3_64 ::calculate(csview).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );
        CHECK( CxxHash3_64 ::calculate(cspan ).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );
        CHECK( CxxHash3_64 ::calculate(string).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );
        CHECK( CxxHash3_64 ::calculate(vector).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );
        CHECK( CxxHash3_64 ::calculate(array ).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );
        CHECK( CxxHash3_64 ::calculate(test  ).get_hexadecimal() == Hexadecimal{"eac0f8990e911168"} );

        CHECK( CxxHash3_128::calculate(csview).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( CxxHash3_128::calculate(cspan ).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( CxxHash3_128::calculate(string).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( CxxHash3_128::calculate(vector).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( CxxHash3_128::calculate(array ).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( CxxHash3_128::calculate(test  ).get_hexadecimal() == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
    }

    SECTION("int32_t data") {
        const int32_t                 carray[10] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::span               span       {carray};
        const std::vector<int32_t>    vector     {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::array<int32_t, 10> array      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        CHECK( CxxHash_32  ::calculate(span  ).get_hexadecimal() == Hexadecimal{"92244ccd"} );
        CHECK( CxxHash_32  ::calculate(vector).get_hexadecimal() == Hexadecimal{"92244ccd"} );
        CHECK( CxxHash_32  ::calculate(array ).get_hexadecimal() == Hexadecimal{"92244ccd"} );

        CHECK( CxxHash_64  ::calculate(span  ).get_hexadecimal() == Hexadecimal{"4dc716196224eb4b"} );
        CHECK( CxxHash_64  ::calculate(vector).get_hexadecimal() == Hexadecimal{"4dc716196224eb4b"} );
        CHECK( CxxHash_64  ::calculate(array ).get_hexadecimal() == Hexadecimal{"4dc716196224eb4b"} );

        CHECK( CxxHash3_64 ::calculate(span  ).get_hexadecimal() == Hexadecimal{"8133e31a5758b115"} );
        CHECK( CxxHash3_64 ::calculate(vector).get_hexadecimal() == Hexadecimal{"8133e31a5758b115"} );
        CHECK( CxxHash3_64 ::calculate(array ).get_hexadecimal() == Hexadecimal{"8133e31a5758b115"} );

        CHECK( CxxHash3_128::calculate(span  ).get_hexadecimal() == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
        CHECK( CxxHash3_128::calculate(vector).get_hexadecimal() == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
        CHECK( CxxHash3_128::calculate(array ).get_hexadecimal() == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
    }

    SECTION("long data") {
        const int64_t                 carray[10] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::span               span       {carray};
        const std::vector<int64_t>    vector     {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::array<int64_t, 10> array      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        CHECK( CxxHash_32  ::calculate(span  ).get_hexadecimal() == Hexadecimal{"cd122fb0"} );
        CHECK( CxxHash_32  ::calculate(vector).get_hexadecimal() == Hexadecimal{"cd122fb0"} );
        CHECK( CxxHash_32  ::calculate(array ).get_hexadecimal() == Hexadecimal{"cd122fb0"} );

        CHECK( CxxHash_64  ::calculate(span  ).get_hexadecimal() == Hexadecimal{"04673d65c892b5ba"} );
        CHECK( CxxHash_64  ::calculate(vector).get_hexadecimal() == Hexadecimal{"04673d65c892b5ba"} );
        CHECK( CxxHash_64  ::calculate(array ).get_hexadecimal() == Hexadecimal{"04673d65c892b5ba"} );

        CHECK( CxxHash3_64 ::calculate(span  ).get_hexadecimal() == Hexadecimal{"a1b53e553e1d1aa9"} );
        CHECK( CxxHash3_64 ::calculate(vector).get_hexadecimal() == Hexadecimal{"a1b53e553e1d1aa9"} );
        CHECK( CxxHash3_64 ::calculate(array ).get_hexadecimal() == Hexadecimal{"a1b53e553e1d1aa9"} );

        CHECK( CxxHash3_128::calculate(span  ).get_hexadecimal() == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
        CHECK( CxxHash3_128::calculate(vector).get_hexadecimal() == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
        CHECK( CxxHash3_128::calculate(array ).get_hexadecimal() == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
    }
}

template< typename XXHASH_T >
Hexadecimal calculate_hash_in_parts(const std::span<const char> data, const std::size_t part_size = 2) {
    XXHASH_T xxh;
    for(std::size_t pos = 0 ; pos < data.size() ; pos += part_size ) {
        const auto data_part = data.subspan(pos, part_size);
        xxh.update(data_part);
    }
    return xxh.get_hash().get_hexadecimal();
}

template< typename XXHASH_T, typename DATA_T >
Hexadecimal calculate_hash_in_parts(const DATA_T& data, const std::size_t part_size = 2) {
    return calculate_hash_in_parts<XXHASH_T>(to_span(data), part_size);
}

TEST_CASE( "CxxHash_*::update(<multitype>)", "[CxxHash]" ) {

    SECTION("char data") {
        const char*                cstring  = "test 1 2 3";
        const std::string_view     cview      {cstring};
        const char                 carray[10] {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::span            cspan      {carray};
        const std::string          string     {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::vector<char>    vector     {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const std::array<char, 10> array      {'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'};
        const Test_t               test      {{'t', 'e', 's', 't', ' ', '1', ' ', '2', ' ', '3'}};

        CHECK( calculate_hash_in_parts<CxxHash_32  >(cview ) == Hexadecimal{"e100d608"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(cspan ) == Hexadecimal{"e100d608"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(string) == Hexadecimal{"e100d608"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(vector) == Hexadecimal{"e100d608"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(array ) == Hexadecimal{"e100d608"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(test  ) == Hexadecimal{"e100d608"} );

        CHECK( calculate_hash_in_parts<CxxHash_64  >(cview ) == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(cspan ) == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(string) == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(vector) == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(array ) == Hexadecimal{"4c54f58f73c928c3"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(test  ) == Hexadecimal{"4c54f58f73c928c3"} );

        CHECK( calculate_hash_in_parts<CxxHash3_64 >(cview ) == Hexadecimal{"eac0f8990e911168"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(cspan ) == Hexadecimal{"eac0f8990e911168"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(string) == Hexadecimal{"eac0f8990e911168"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(vector) == Hexadecimal{"eac0f8990e911168"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(array ) == Hexadecimal{"eac0f8990e911168"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(test  ) == Hexadecimal{"eac0f8990e911168"} );

        CHECK( calculate_hash_in_parts<CxxHash3_128>(cview ) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(cspan ) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(string) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(vector) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(array ) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(test  ) == Hexadecimal{"9be078a972ebcf54286980cb2db2515f"} );
    }

    SECTION("int32 data") {
        const int32_t                 carray[10] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::span               cspan      {carray};
        const std::vector<int32_t>    vector     {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::array<int32_t, 10> array      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        CHECK( calculate_hash_in_parts<CxxHash_32  >(cspan ) == Hexadecimal{"92244ccd"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(vector) == Hexadecimal{"92244ccd"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(array ) == Hexadecimal{"92244ccd"} );

        CHECK( calculate_hash_in_parts<CxxHash_64  >(cspan ) == Hexadecimal{"4dc716196224eb4b"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(vector) == Hexadecimal{"4dc716196224eb4b"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(array ) == Hexadecimal{"4dc716196224eb4b"} );

        CHECK( calculate_hash_in_parts<CxxHash3_64 >(cspan ) == Hexadecimal{"8133e31a5758b115"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(vector) == Hexadecimal{"8133e31a5758b115"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(array ) == Hexadecimal{"8133e31a5758b115"} );

        CHECK( calculate_hash_in_parts<CxxHash3_128>(cspan ) == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(vector) == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(array ) == Hexadecimal{"30b8eaf2d6b89654f715e1d1b85cd822"} );
    }

    SECTION("int64 data") {
        const int64_t                 carray[10] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::span               cspan      {carray};
        const std::vector<int64_t>    vector     {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        const std::array<int64_t, 10> array      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        CHECK( calculate_hash_in_parts<CxxHash_32  >(cspan ) == Hexadecimal{"cd122fb0"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(vector) == Hexadecimal{"cd122fb0"} );
        CHECK( calculate_hash_in_parts<CxxHash_32  >(array ) == Hexadecimal{"cd122fb0"} );

        CHECK( calculate_hash_in_parts<CxxHash_64  >(cspan ) == Hexadecimal{"04673d65c892b5ba"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(vector) == Hexadecimal{"04673d65c892b5ba"} );
        CHECK( calculate_hash_in_parts<CxxHash_64  >(array ) == Hexadecimal{"04673d65c892b5ba"} );

        CHECK( calculate_hash_in_parts<CxxHash3_64 >(cspan ) == Hexadecimal{"a1b53e553e1d1aa9"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(vector) == Hexadecimal{"a1b53e553e1d1aa9"} );
        CHECK( calculate_hash_in_parts<CxxHash3_64 >(array ) == Hexadecimal{"a1b53e553e1d1aa9"} );

        CHECK( calculate_hash_in_parts<CxxHash3_128>(cspan ) == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(vector) == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
        CHECK( calculate_hash_in_parts<CxxHash3_128>(array ) == Hexadecimal{"fe06f302eaf7f6c1071bb76f840473e3"} );
    }
}

// This is slower on the x86_64 systems I tested.
[[nodiscard]] inline constexpr auto alternate_cmp(const ::CxxHash::CxxHash3_128::Hash_t hash_a, const ::CxxHash::CxxHash3_128::Hash_t hash_b) noexcept {
    const bool hab = hash_a.high64 > hash_b.high64;
    const bool hba = hash_b.high64 > hash_a.high64;
    if(hab != hba) {
        return static_cast<int>(hab) - static_cast<int>(hba);
    }
    const bool lab = hash_a.low64 > hash_b.low64;
    const bool lba = hash_b.low64 > hash_a.low64;
    return static_cast<int>(lab) - static_cast<int>(lba);
}

TEST_CASE("benchmark CxxHash3_128::operator<=>", "[CxxHash]") {
    std::random_device random_device;
    std::mt19937 random_generator(random_device());
    std::uniform_int_distribution<uint64_t> random_distribution(std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max());

    const auto generator = [&]() {
        return CxxHash3_128::Hash_t{random_distribution(random_generator), random_distribution(random_generator)};
    };

    constexpr std::size_t count = 1000;
    std::vector<CxxHash3_128::Hash_t> hashes_a{count};
    std::ranges::generate(hashes_a, generator);

    std::vector<CxxHash3_128::Hash_t> hashes_b{count};
    std::ranges::generate(hashes_b, generator);

    BENCHMARK("benchmark <=> with collisions") {
        int sum = 0;
        for(const CxxHash3_128::Hash_t hash_a : hashes_a) {
            for(const CxxHash3_128::Hash_t hash_b : hashes_a) {
                sum += hash_a <=> hash_b;
            }
        }
        return sum;
    };

    BENCHMARK("benchmark <=> with collisions (alternate with conditional jump)") {
        int sum = 0;
        for(const CxxHash3_128::Hash_t hash_a : hashes_a) {
            for(const CxxHash3_128::Hash_t hash_b : hashes_a) {
                sum += alternate_cmp(hash_a, hash_b);
            }
        }
        return sum;
    };

    BENCHMARK("benchmark <=> no collisions") {
        int sum = 0;
        for(const CxxHash3_128::Hash_t hash_a : hashes_a) {
            for(const CxxHash3_128::Hash_t hash_b : hashes_b) {
                sum += hash_a <=> hash_b;
            }
        }
        return sum;
    };

    BENCHMARK("benchmark <=> no collisions (alternate with conditional jump)") {
        int sum = 0;
        for(const CxxHash3_128::Hash_t hash_a : hashes_a) {
            for(const CxxHash3_128::Hash_t hash_b : hashes_b) {
                sum += alternate_cmp(hash_a, hash_b);
            }
        }
        return sum;
    };

}
