cmake_minimum_required(VERSION 3.14)

project(CxxHash LANGUAGES CXX)

set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Catch2 2 REQUIRED)

function(setup_target TARGET)
  target_link_libraries(${TARGET} PRIVATE xxhash)

  if(CMAKE_BUILD_TYPE STREQUAL "Release")
      target_compile_definitions(${TARGET} PRIVATE _FORTIFY_SOURCE=3)
  endif()

  if(CMAKE_BUILD_TYPE STREQUAL "Debug" AND CMAKE_SYSTEM_NAME STREQUAL "Linux" AND CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      target_compile_options(${TARGET} PRIVATE -fprofile-arcs -ftest-coverage)
      target_link_options   (${TARGET} PRIVATE -fprofile-arcs -ftest-coverage -lgcov)
      target_compile_options(${TARGET} PRIVATE -fsanitize=kernel-address -fsanitize=leak -fsanitize=undefined -fsanitize-address-use-after-scope)
      target_link_options   (${TARGET} PRIVATE -fsanitize=kernel-address -fsanitize=leak -fsanitize=undefined -fsanitize-address-use-after-scope -lasan)
  endif()

  if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      target_compile_options(${TARGET} PRIVATE -Wall -Wextra -Wpedantic -Wconversion -Wshadow -Werror -flto)
      target_link_options(${TARGET} PRIVATE -flto=auto)
  endif()
endfunction()

add_executable(CxxHash-example
  Hexadecimal.h
  CxxHash.h
  example.cpp
)

setup_target(CxxHash-example)

add_executable(CxxHash-tests
  Hexadecimal.h
  CxxHash.h
  tests.cpp
)

setup_target(CxxHash-tests)
