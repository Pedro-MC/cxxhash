# CxxHash - C++ Wrapper for xxHash

**CxxHash** is a C++ wrapper for [**xxHash**](https://github.com/Cyan4973/xxHash).

It is composed of two header files that just need to be #include"d" to be used.

## Dependencies

**CxxHash** depends only on C++ standard library and [**xxHash**](https://github.com/Cyan4973/xxHash).

## Building

Headers only so no build required.

## Tests

The tests are in the tests.cpp file and use [Catch2](https://github.com/catchorg/Catch2).

To build and run the tests do:
```shell
git clone https://gitlab.com/Pedro-MC/cxxhash.git
cd CxxHash
mkdir build
cmake -S . -B build
cd build
make
./CxxHash-tests
```

## Examples

See the file [example.cpp](https://gitlab.com/Pedro-MC/cxxhash/-/blob/main/example.cpp).

## Bugs and feature requests

If you find any bugs or have any feature requests please [open an issue](https://gitlab.com/Pedro-MC/cxxhash/-/issues).

## Contributions

Contributions are welcomed.

See [AUTHORS](https://gitlab.com/Pedro-MC/cxxhash/-/blob/main/AUTHORS.md) for a list of contributors.

## License

**CxxHash** is licensed under the [BSD 2-Clause License](https://gitlab.com/Pedro-MC/cxxhash/-/blob/main/LICENSE).

Please contact the copyrights holder(s) if you are interested in a commercial license.
