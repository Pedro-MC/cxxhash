/*
 * CxxHash - C++ Wrapper for xxHash (Extremely Fast Hash algorithm)
 * Header File
 * Copyright © 2023-2023 Pedro Miguel Carvalho <PedroMC@pmc.com.pt>
 *
 * BSD 2-Clause License (https://www.opensource.org/licenses/bsd-license.php)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the
 *      distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You can contact the author at: mailto:PedroMC@pmc.com.pt?subject=CxxHash
 */

#pragma once
#ifndef HEXADECIMAL_H
#define HEXADECIMAL_H

#include <algorithm>
#include <array>
#include <cstddef>
#include <limits>
#include <span>
#include <stdexcept>
#include <string>
#include <utility>

namespace CxxHash {

class Hexadecimal {
public:

    explicit Hexadecimal(const std::span<const unsigned char> data) : _hexadecimal{_to_hexadecimal(data)} {}
    explicit Hexadecimal(const std::string& hexadecimal) : _hexadecimal{_check_hexadecimal(std::string{hexadecimal})} {}
    explicit Hexadecimal(std::string&& hexadecimal) : _hexadecimal{_check_hexadecimal(std::move(hexadecimal))} {}

    Hexadecimal() = default;
    Hexadecimal(const Hexadecimal&) = default;
    Hexadecimal(Hexadecimal&&) noexcept = default;
    ~Hexadecimal() = default;

    Hexadecimal& operator=(const Hexadecimal&) = default;
    Hexadecimal& operator=(Hexadecimal&&) noexcept = default;

    [[nodiscard]] bool operator==(const Hexadecimal&) const noexcept = default;
    [[nodiscard]] int operator<=>(const Hexadecimal&) const noexcept = default;

    [[nodiscard]] static bool is_valid_hexadecimal(const std::string& hexadecimal) noexcept {
        return hexadecimal.size() % 2 == 0 &&
            std::find_if(hexadecimal.cbegin(), hexadecimal.cend(), [](const char C) {
                // hexadecimal_to_value[C] guaranteed to be valid since 0 <= C <= std::numeric_limits<unsigned char>::max()>.
                return _hexadecimal_to_value[static_cast<unsigned char>(C)] == _invalid_hexadecimal;
            }) == hexadecimal.cend();
    }

    [[nodiscard]] std::string get_string() const noexcept {
        return _hexadecimal;
    }

    [[nodiscard]] std::size_t size() const noexcept {
        return _hexadecimal.size();
    }

    [[nodiscard]] std::size_t data_size() const noexcept {
        return _hexadecimal.size() / 2;
    }

    Hexadecimal& from(const std::span<const unsigned char> data) {
        _hexadecimal = _to_hexadecimal(data);
        return *this;
    }

    const Hexadecimal& to(std::span<unsigned char> data) const {
        if(data.size() * 2 != _hexadecimal.size()) {
            throw std::invalid_argument{"Invalid data size."};
        }
        bool have_pair = false;
        unsigned char previous = 0;
        auto iter = data.begin();
        for(const char C : _hexadecimal) {
            // hexadecimal_to_value[C] guaranteed to be valid since 0 <= C <= std::numeric_limits<unsigned char>::max().
            const unsigned char value = _hexadecimal_to_value[static_cast<unsigned char>(C)];
            if(have_pair) {
                *iter = previous | value;
                ++iter;
            } else {
                previous = static_cast<unsigned char>(value << 4U);
            }
            have_pair = ! have_pair;
        }
        return *this;
    }

private:

    [[nodiscard]] static std::string _check_hexadecimal(std::string&& hexadecimal) {
        if(! is_valid_hexadecimal(hexadecimal)) {
            throw std::invalid_argument{"Invalid hexadecimal string."};
        }
        return hexadecimal;
    }

    [[nodiscard]] static std::string _to_hexadecimal(const std::span<const unsigned char> data) {
        static constexpr const auto value_to_hexadecimal = std::array<char, 16>{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        std::string str;
        str.reserve(data.size() * 2);
        for(const unsigned char C : data) {
            const unsigned char high_value = C >> 4U;
            const unsigned char low_value = C & 0x0fU;
            // value_to_hexadecimal[(high|low)_value] guaranteed to be valid since 0 <= (high|low)_value <= 15.
            str.push_back(value_to_hexadecimal[high_value]);
            str.push_back(value_to_hexadecimal[low_value]);
        }
        return str;
    }

    std::string _hexadecimal;

    static constexpr const unsigned char _invalid_hexadecimal = std::numeric_limits<unsigned char>::max();
    static constexpr const auto _hexadecimal_to_value = []() noexcept {
        std::array<unsigned char, std::numeric_limits<unsigned char>::max() + 1> array{};
        array.fill(_invalid_hexadecimal);
        array['0'] = 0;
        array['1'] = 1;
        array['2'] = 2;
        array['3'] = 3;
        array['4'] = 4;
        array['5'] = 5;
        array['6'] = 6;
        array['7'] = 7;
        array['8'] = 8;
        array['9'] = 9;
        array['a'] = 10;
        array['b'] = 11;
        array['c'] = 12;
        array['d'] = 13;
        array['e'] = 14;
        array['f'] = 15;
        array['A'] = 10;
        array['B'] = 11;
        array['C'] = 12;
        array['D'] = 13;
        array['E'] = 14;
        array['F'] = 15;
        return array;
    }();
};

} // namespace CxxHash

#endif // HEXADECIMAL_H
