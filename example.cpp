/*
 * CxxHash - C++ Wrapper for xxHash (Extremely Fast Hash algorithm)
 * Header File
 * Copyright © 2023-2023 Pedro Miguel Carvalho <PedroMC@pmc.com.pt>
 *
 * BSD 2-Clause License (https://www.opensource.org/licenses/bsd-license.php)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the
 *      distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You can contact the author at: mailto:PedroMC@pmc.com.pt?subject=CxxHash
 */

#include <array>
#include <iostream>
#include <span>
#include <string>
#include <vector>

#include "CxxHash.h"

using CxxHash::CxxHash_32;
using CxxHash::CxxHash_64;
using CxxHash::CxxHash3_64;
using CxxHash::CxxHash3_128;

/** Returns a hexadecimal string representation of the hash of the passed string. */
template< typename XXHASH_T >
std::string xxHash(const auto& string) {
    return XXHASH_T::calculate(string).get_hexadecimal().get_string();
}

int main() {
    const char c_string[] = "Turn the world to hash!";
    const auto span = std::span{c_string};
    const auto array = std::to_array(c_string);
    const auto string = std::string{std::begin(c_string), std::end(c_string)};
    const auto vector = std::vector<char>{std::begin(c_string), std::end(c_string)};

    std::cout << c_string << '\n'
              << '\n';

    std::cout << "xxHash_32" << '\n'
              << "std::span   : " << xxHash<CxxHash_32>(span  ) << '\n'
              << "std::array  : " << xxHash<CxxHash_32>(array ) << '\n'
              << "std::string : " << xxHash<CxxHash_32>(string) << '\n'
              << "std::vector : " << xxHash<CxxHash_32>(vector) << '\n'
              << '\n';

    std::cout << "xxHash_64" << '\n'
              << "std::span   : " << xxHash<CxxHash_64>(span  ) << '\n'
              << "std::array  : " << xxHash<CxxHash_64>(array ) << '\n'
              << "std::string : " << xxHash<CxxHash_64>(string) << '\n'
              << "std::vector : " << xxHash<CxxHash_64>(vector) << '\n'
              << '\n';

    std::cout << "xxHash3_64" << '\n'
              << "std::span   : " << xxHash<CxxHash3_64>(span  ) << '\n'
              << "std::array  : " << xxHash<CxxHash3_64>(array ) << '\n'
              << "std::string : " << xxHash<CxxHash3_64>(string) << '\n'
              << "std::vector : " << xxHash<CxxHash3_64>(vector) << '\n'
              << '\n';

    std::cout << "xxHash3_128" << '\n'
              << "std::span   : " << xxHash<CxxHash3_128>(span  ) << '\n'
              << "std::array  : " << xxHash<CxxHash3_128>(array ) << '\n'
              << "std::string : " << xxHash<CxxHash3_128>(string) << '\n'
              << "std::vector : " << xxHash<CxxHash3_128>(vector) << '\n'
              << '\n';

    return 0;
}
